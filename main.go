package main

import (
	"email-service/api"
	"email-service/common"
	"email-service/config"
	"email-service/env"
	"email-service/handlers"
	"email-service/model"
	"github.com/cloudevents/sdk-go/v2/event"
	"github.com/gin-gonic/gin"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core"
	"log"
	"os"
)

var logger = common.GetLogger()

func init() {
	err := config.LoadConfig()
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	envir := env.GetEnv()
	router := core.NewServer(envir)
	router.Add(func(group *gin.RouterGroup) {
		api.EmailRoute(group)
	})
	addBrokerSubscription(envir)

	err := router.Run(config.ServerConfiguration.Port)
	if err != nil {
		logger.Error(err, "server failed")
		os.Exit(1)
	}
}

func addBrokerSubscription(envir common.Env) {
	envir.BrokerSubscribe(config.ServerConfiguration.Nats.TopicReceive, func(e event.Event) {
		var eml model.EmailData
		err := e.DataAs(&eml)
		if err != nil {
			logger.Error(err, "error during handling cloudevents subscription")
		}
		err = handlers.SendEmail(&eml)
		if err != nil {
			logger.Error(err, "error during handling cloudevents subscription")
		}
	})
}
