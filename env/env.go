package env

import (
	"context"
	"email-service/common"
	"email-service/config"
	"email-service/connection"
	"email-service/services"
	"github.com/cloudevents/sdk-go/v2/event"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/messaging/cloudeventprovider"
)

var logger = common.GetLogger()

func init() {
	env = &EnvObj{}
}

func GetEnv() common.Env {
	return env
}

var env *EnvObj

type EnvObj struct {
	brokers map[string]*cloudeventprovider.CloudEventProviderClient
}

func (env *EnvObj) IsHealthy() bool {
	return services.GetSMTP() != nil
}

func (env *EnvObj) GetBroker(topic string) *cloudeventprovider.CloudEventProviderClient {
	return env.brokers[topic]
}
func (env *EnvObj) BrokerSubscribe(topic string, handler func(e event.Event)) {
	if config.ServerConfiguration.Nats.Url != "" {
		broker, err := connection.CloudEventsConnection(topic, cloudeventprovider.ConnectionTypeSub)
		if err != nil {
			logger.Error(err, "connection failed", "topic", topic)
			return
		}
		env.brokers[topic] = broker
		go func() {
			er := broker.SubCtx(context.Background(), handler)
			if er != nil {
				logger.Error(er, "subscription failed", "topic", topic)
			}
		}()
	}
}
