package common

import (
	"email-service/config"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/types"
	"log"
	"sync"
)

var logger types.Logger

var once sync.Once

func GetLogger() types.Logger {
	once.Do(initLogger)
	return logger
}

func initLogger() {
	l, err := core.NewLogger(config.ServerConfiguration.LogLevel, config.ServerConfiguration.IsDev)
	if err != nil {
		log.Fatal(err)
	}
	logger = *l
}
